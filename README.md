# DEZSYS_GK732_WINDPARK_REST

## Einfuehrung

Eine Windkraftanlage erzeugt Daten über die Stromerzeugung und den Umgebungsbedingungen in regelmaessigen Zeitabstaenden. Diese Daten sollen in einer standardisierten Form zur weiteren Verarbeitung zur Verfügung gestellt werden.

## Voraussetzungen

* Java Programmierkenntnisse
* Verwendung von Maven und Git
* Grundlagen Dezentrale Systeme
* Grundlagen zu XML, JSON & REST

## Aufgabenstellung

Entwickeln Sie einen Simulator der die Daten der Windkraftanlage generiert. Es ist dabei zu achten, dass die Daten realistisch sind und im Zusammenhang mit einer entsprechenden Einheit erzeugt werden. 

Die Daten der Windkraftanalage sollen ueber einer REST Schnittstelle veroeffentlicht werden. Die Schnittstelle verwendet standardmaessig das JSON Format und kann optional auf XML umgestellt werden.

Die Schnittstelle soll mit einer einfachen Applikation getestet werden. Dabei sollen die Daten mit Hilfe von HTML und JQuery "konsumiert" und in einer Tabelle dargestellt werden.

## Erweiterung

Die Daten sollen vor einem nicht autorisierten Zugriff geschützt werden. Entwerfen Sie und implementieren Sie ein Konzept, um die Datensicherheit zu gewährleisten.

## Aufbau der Applikation

- Windengine Simulation
- Windengine Interface
- Windengine Model (JSON Format)
- Controller receive HTTP requests
- Service contains business logic
- Controller publish windengine data requested by client
- Client consumes windengine data 

## Technologien

- Springboot
- Maven
- REST
- JSON
- jQuery

## Dokumente und Links

Spring Boot   
https://spring.io/projects/spring-boot

Building an Application with Spring Boot  
https://spring.io/guides/gs/spring-boot/

Spring Initializr  
https://start.spring.io/

Understanding REST  
https://spring.io/understanding/REST

Building a RESTful Web Service  
https://spring.io/guides/gs/rest-service/

Consuming a RESTful Web Service  
https://spring.io/guides/gs/consuming-rest/

Consuming a RESTful Web Service with jQuery
https://spring.io/guides/gs/consuming-rest-jquery/

## Bewertung

Gruppengroesse: 1 Person   
Anforderungen "ueberwiegend erfuellt"    

* Windengine Simulator
* Implementation der REST Schnittstelle
* Daten im JSON Format zur Verfuegung stellen
* Implementation eines "Consumer" und Darstellung der Daten in einer Tabelle
* Konzept fuer ein entsprechendes Sicherheitskonzept fuer die REST Schnittstelle

Anforderungen "zur Gaenze erfüllt"

* Daten im XML Format zur Verfuegung stellen
* Umsetzung eines Sicherheitskonzepts fuer die REST Schnittstelle


## Anwendung

Dieser Teil beschreibt die Arbeitsdurchführung, persönliche Erfahrungen und den derzeitigen Stand.

### Import

Der erste Schritt war das Aufsetzen des Projektes. Unter Windows ist die 
Maven-Unterstützung nicht so gut, doch zu meinen Glück besitzt die IntelliJ-IDE
 eine gute interne Maven-Kompatibilität und dadurch konnte ich das Git-Repo 
 einfach als Maven-Projekt importieren. 
 
Nach dem erfolgreichen Importieren konnte ich einfach die Spring-Boot-Application ausführen und im Browser
"localhost:8080" eingeben. Auf dieser Seite hatte ich dann Zugriff auf die Zufallsdaten einer Windanlage in der Form von 
XML. 

### Ausgabe von JSON

Der nächste Schritt war das Anzeigen der Daten in Form von JSON. Dazu gibt es das Tutorial auf dieser 
[Website][1]. Dort erfährt man, dass man zusätzlich noch zu den XML-Dependencies die JSON-Dependencies angeben
 soll. Doch selbst nach Befolgen der Anleitung bekam ich immer noch eine XML-Ausgabe. Ein Mitschüler von mir,
Herr Manuel Kisser, fand aber eine Lösung, man muss beim Request-Mapping mit [produces][2] angeben, dass man eine JSON-Ausgabe
will:
```java
@RequestMapping(value = "/windengine/{windengineID}/data_json", produces = MediaType.APPLICATION_JSON_VALUE)
```
Danach habe ich wieder eine XML-Ausgabe bekommen, nicht weil der Code falsch war, sondern weil die Standard-Ausführung
bei IntelliJ die neuen Dependencies nicht berücksichtigt hat. Um endlich zur Erfolgreichen JSON-Ausgabe zu kommen
 musste ich noch folgende Maven-Anweisung angeben:
```
mvn clean package spring-boot:run
``` 

### Consumer (Darstellung der Daten in einer Tabelle)

Für das Erstellen der Consumer-Funktion habe ich diese [Anleitung][3] benutzt. Eigentlich ist die Aufgabe nicht
schwer, man muss nur eine html-Seite mit einer Tabelle erstellen die eine JavaScript-Datei einliest, die wiederum per 
JQuery die Daten von der REST-Schnittstelle einliest. Doch nachdem ich das Konzept umgesetzt hatte, bekam ich immer
die selbe Fehlermeldung:
![Fehler](img/img_1.png)
```
No 'Access-Control-Allow-Origin' header is present on the requested resource.
```
Nach kurzer Recherche verstand ich das Problem: Aus Sicherheitsgründen werden alle Requests an die 
REST-Schnittstelle geblockt, da die Domain, in diesem Fall die Port-Nummer, nicht identisch sind. Zum Glück gibt 
es für das Erlauben von Cross-Origin eine [Dokumenatation][4] von Spring. Für das Erlauben der Verbindung muss man im Controller
folgendes angeben:
```
@CrossOrigin(origins = "*")
```
Danacht wollte ich die Website des Consumers(consumer.html) auf der Hautpseite "localhost:8080" verlinken. In WindengineData habe 
ich daher ein neues Request-Mapping für diese Seite mit passender Funktion erstellt. Doch zu meinem Erstauenen
wurde nicht die Website, sondern einfach consumer ausgegeben. In der Dokumenatation von Spring habe ich Erfahren, dass
RestController immer nur JSON/XML, einfach Daten, zurückgibt da jede Funktion ein ResponseBody besitzt. Für meinen
Zweck eine Website zurückzugegebn brauchte ich einen einfachen Controller. Daher habe ich eine neue Klasse "WindengineControllerExt"
erstellt und dort ein Request-Mapping für den Consumer erstellt. Im WindengineController habe ich noch einen Link für die Consumer-Seite 
ergänzt. Dann habe die Applikation gestartet und es funktionierte. Da die Consumer-Website jetzt die gleiche Domain wie die REST-Schnittstelle
besitzt, ist Cross-Origin eigentlich nicht mehr vonnöten.

### Sicherheitskonzept

#### Theorie

Um die ganze Anwendung zu Schützen würde ich ein Login-System implementieren, wo erst Benutzer nach erfolgreichem Anmelden
Einsicht in die Daten der Windanlage haben. Auf die Idee zu diesem Konzept bin ich gekommen, als ich diese [Anleitung][5] 
von Spring gelesen habe, die beschreibt wie man mit Spring Web Security dieses Konzept umsetzt. Dazu gibt es auch einen [Artikel][6],
 der näher auf die Architektur von Spring Web Security eingeht. Ganz grob gesagt benuzt die Web Security einfache Filter, die für bestimmte Rollen
 und Aufgaben definiert werden. Diese können dann mit einer Konfigurations-Klasse beeinflusst werden. 
![Architektur](img/img_4.png)
Was aber nirgendwo beschrieben wird, ist wie man das Login-System ausweiten kann, damit man dann Dateien bzw. ganze Domains, die nicht mit
"localhost:8080" beginnen, schützen kann. Dann muss man wahrscheinlich dem Servlet die Benutzerdaten senden, in ihm auf Korrektheit prüfen und
dann wie z.B. im Consumer per JQuery diese Daten zurück erhalten.

#### Umsetzung

Die Umsetzung des Konzeptes erfolgte auf Basis der oberen Anleitung, ich musste halt diese nur an das Projekt anpassen. Zu aller erst
musste man in der "pom.xml"-Datei die Dependencies für die Spring Web Security angeben. Danach musste man zwei Klassen hinzufügen, 
MvcConfig und WebSecurityConfig, außerdem war eine "login.html" für das Login-Fenster von Nöten. MvcConfig wir benötigt, da die 
Web-Anwendung auf Spring MVC basiert und View-Controller für die "html"-Dateien wie "login.html" benötigt werden. Die Datei
WebSecurityConfig bildet den Filter für jegliche http-Request und in dieser muss angegeben werden, welche Seiten vom Login-System nicht geschützt
werden sollen, welsche schon und was beim Login und Logout geschehen soll.

Bei der Umsetzung hatte ich anfangs ein bisschen Probleme, weil meine html-Dateien im falschen Ordner waren und dadurch immer der Fehler kam,
dass sie nicht gefunden werden können. Nachdem ich sie in resources/templates verschoben hatte, kam dann auch die Login-Page und ich
konnte mich anmelden. Falls man im nicht angemeldeten Zustand eine Seite aufmachen wollte, kam immer die Login-Seite zum Vorschau und
somit wurde die Seite geschützt. Was aber nicht funktionierte, war das ausloggen. Ich wusste lange nicht woran das liegt, aber dann habe ich 
bemerkt, dass dieser Code
```HTML
<form th:action=\"@{/logout}\" method=\"post\">
```

von der dynamisch erzeugten Startseite nicht funktionierte. Somit baute ich die Startseite in einer normalen html-Datei nach und gab
sie nicht vom RestController, sonder von meinem zweiten normalen Controller zurück. Und tatsächlich, das Login und Logout funktionierte.

Was ich als nächstes Implementieren will, ist, dass man sich von außen authentifizieren kann und dann Zugriff auf die Windanlage-Daten 
bekommt. Dazu habe ich eine html-Datei "extern.html" und eine JavaScript-Datei "externJS.js" erstellt und probiert per AJAX zum Testen
irgendwelche Strings zu Senden und zu Empfangen. Doch leider spuckt mir die Funktion dauern CORS-Fehler aus, obwohl ich Cross-Origin
laut meinem Wissen erlaubt habe. Was auch komisch ist, dass der Fehler sagt, dass er die Seite "/consumer_login", die für das externe 
Authentifizieren gedacht ist, zu der Login-Page umleitet, obwohl ich sage, dass diese Seite nicht durch das Login-System geschützt werden 
soll. 
![Ein alt bekannter Fehler](img/img_5.png)

[1]: https://spring.io/guides/gs/rest-service/
[2]: https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/RequestMapping.html#produces--
[3]: https://spring.io/guides/gs/consuming-rest-jquery/
[4]: https://spring.io/guides/gs/rest-service-cors/
[5]: https://spring.io/guides/gs/securing-web/#_summary
[6]: https://spring.io/guides/topicals/spring-security-architecture/
