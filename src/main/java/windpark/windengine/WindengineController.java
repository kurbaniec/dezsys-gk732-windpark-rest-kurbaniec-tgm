package windpark.windengine;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import windpark.model.WindengineData;

import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;


@RestController
public class WindengineController {

    @Autowired
    private WindengineService service;


    /**
    @RequestMapping("/")
    public String windengineMain() {
    	String mainPage = "This is the windengine application! (DEZSYS_GK72_WINDPARK) <br/><br/>" +
                          "<a href='http://localhost:8080/windengine/001/data_xml'>Link to windengine/001/data_xml</a><br/>" +
                          "<a href='http://localhost:8080/windengine/001/data_json'>Link to windengine/001/data_json</a><br/>" +
                          "<a href='http://localhost:8080/windengine/001/transfer'>Link to windengine/001/transfer</a><br/>" +
                          "<a href='/consumer'>Link to Consumer</a><br/>" +
                          "<form th:action=\"@{/logout}\" method=\"post\">\n" +
                          "   <input type=\"submit\" value=\"Sign Out\"/>\n" +

                          "</form>";
        return mainPage;
    }*/



    @RequestMapping(value = "/windengine/{windengineID}/data_xml", produces = MediaType.APPLICATION_XML_VALUE)
    public WindengineData windengineDataXML( @PathVariable String windengineID ) {
        return service.getWindengineData( windengineID );
    }


    //@CrossOrigin(origins = "http://localhost:63342")
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/windengine/{windengineID}/data_json", produces = MediaType.APPLICATION_JSON_VALUE)
    public WindengineData windengineDataJSON( @PathVariable String windengineID ) {
        return service.getWindengineData( windengineID );
    }

    @RequestMapping("/windengine/{windengineID}/transfer")
    public String windengineTransfer( @PathVariable String windengineID ) {
        return service.getGreetings("Windengine.Transfer!");
    }

    // Methode um externes Authentifizieren ermöglichen zu können
    // Funktioniert noch nicht
    @CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
    @RequestMapping(value = "/consumer_login")
    public  @ResponseBody String  getSearchUserProfiles(@RequestParam String pName, @RequestParam String lName) {
        String pNameParameter = pName;
        String lNameParameter = lName;
        System.out.println("test");
        return "zwei";
        // your logic next

    }
    
}